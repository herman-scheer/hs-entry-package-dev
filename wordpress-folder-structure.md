# **WordPress Folder Structure**  

```sh
|-- css/  
|-- icons/
    |-- color.svg
    |-- no-color.svg
|-- images/
|-- layout/
    |-- page-layout.php
    |-- sidebar-layout.php
    |-- full-width-layout.php
|-- pages/
    |-- 404.php
    |-- archive.php
    |-- home-page.php
    |-- about-page.php
    |-- contact-page.php
|-- parts/
    |-- loop-single-part.php
    |-- comments-part.php
    |-- footer.php
    |-- header.php
    |-- nav-offcanvas-part.php
    |-- sidebar.php
|-- scripts/  
|-- src/
    |-- icons/
        |-- color/
            |-- icons1.svg
            |-- icon2.svg
        |-- no-color/
            |-- icon3.svg
            |-- icon4.svg
    |-- images/
        |-- favicon.ico
        |-- logo.png
    |-- sass/
        |-- base/
            |-- global.sass
        |-- layout/
            |-- page-layout.sass
            |-- sidebar-layout.sass
            |-- full-width-layout.sass
            |-- single-layout.sass
        |-- pages/
            |-- home-page.sass
            |-- about-page.sass
            |-- contact-page.sass
        |-- parts/
            |-- loop-single-part.sass
            |-- comments-part.sass
            |-- nav-offcanvas-part.sass
        |-- utilities/
            |-- buttons.sass
            |-- extends.sass
            |-- forms.sass
            |-- functions.sass
            |-- maps.sass
            |-- mixins.sass
            |-- typography.sass
        |-- vendors/
            |-- foundation-sites.sass
        |-- main.sass
        |-- _settings.sass
    |-- scripts/
        |-- components/  /*optional*/
        |-- states/  /*optional*/
        |-- vendor/  /*optional*/
        |-- main.js
|-- tasks/
    |-- default.js
    |-- icons.js
    |-- images.js
    |-- scripts.js
    |-- styles.js
    |-- watch.js
|-- .eslintrc
|-- .sass-lint.yml
|-- gulpfile.js
|-- index.php
|-- package.json
|-- screenshot.png
|-- style.css


```