# **Herman-scheer Entry Package - Dev**

**Resources**  
1. [Sass Guidelines]  
2. [Foundation Framework]  
3. [Jade Introduction]  
4. [Jade Tutorial]  
5. [BEM Methodology]  
6. [BEM Naming Conventions]  
7. [Get BEM - intro]  

[Sass Guidelines]: <https://bitbucket.org/herman-scheer/hs-entry-package-dev/src/c5e4772a169b22ea8925d95d0fc6754eff146111/sass-guidelines.md?fileviewer=file-view-default>
[Foundation Framework]: <http://foundation.zurb.com/sites/docs/>
[Jade Introduction]: <http://webapplog.com/jade/>
[Jade Tutorial]: <http://jade-lang.com/tutorial/>
[BEM Methodology]: <https://en.bem.info/method/key-concepts/>
[BEM Naming Conventions]: <https://en.bem.info/method/naming-convention/>
[Get BEM - intro]: <http://getbem.com/introduction/>

