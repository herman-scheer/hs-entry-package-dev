# **Sublime Project Settings** 

Please use these project specific settings in Sublime to insure code formatting consistency between our in-house and outsourced projects. Follow the step below to include project specific settings.

1: Install Sublime pacakge 'Project Manager'.  
`cmd + shift + p` > `install package` > `Project Manger`  

2: Add your project with Project Manger. Make sure you have the project folder open in Sublime.  
`Project` > `Project Manager` > `Project Manager: Add Project`  

3: Edit your project.  
`Project` > `Project Manger` > `Project Manager: Edit Project` 

4: Replace the project settings with the `.sublime-project-sample.json` file except for the folder path at the top. 