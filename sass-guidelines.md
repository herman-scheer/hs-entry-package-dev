# **Herman-Scheer Sass Guildines**

**Table of contents**  
1. CSS Property Order  
2. BEM + ECSS + Naming Convention  
3. Sass Formatting  
4. Comments  

## **1. CSS Property Order**
We'll be using the concentric css property order used by our [Sass linter]. Visit [this page] for the complete list of the property order.

## **2. BEM + ECSS + Naming Conventions**
We use the BEM methdology for our component classes. There are some minor tweaks we've made to the method, which we'll cover below. Visit [Get BEM] to learn more about this method. We only use a small part ECSS, but you can read more about it [here].

### **BEM**
Here's a quick example of how the component class should look using BEM.

```sh
.block

.block__element

.block--modifier / .block__element--modifier
```

### **ECSS**
Here's how to add context based changes to a component
```sh
.parent &
```

And here's an example of how you Sass should look with the above methods combined

```sh
.widget
    display: inline-block
    width: 20rem
    background-color: $theme-light
    
    &__title
        color: $color-one
    
    &__title--alert
        border: 1px solid $alert
        color: $alert
    
    // module/component modifiers go at the end
    &--theme-dark
        background-color: $theme-dark
    
    // context based modifiers should go after module/component specific modifiers
    .sidebar &
        width: 100%
```
### **Naming Convention**


##### Jade
When naming the elements of a component stick with assigning 1 element per block. For example...

```sh
// jade file

form.form

  .form__field

    label.form__label

    input.form__input
```

Notice that even though the label is inside `.form__field` we don't name the label element `.form__field__label`.

##### Sass
This is how the Sass should look for the above form component
```sh
// Sass file

.form

    &__field
    
    &__label
    
    &__input
```
  
##### How To Utilize BEM Between Your Jade and Sass
Notice in the above Sass file we used a shallow nesting approach. This may look a bit odd at first but it comes with great advantages. Let's take a look at a more traditional method of writing styles with the same form component with example styling.

```sh
.form
    display: block
    
    .form__field
        width: 100%
        
        .form__label
            font-weight: bold
        
        .form__input
            border: 1px solid $color-one
    
```

Although there's nothing techinically wrong with writing your styles this way there is one big caveat to this method. Say you need to move the `.form__label` element outside of the `.form__field` element. Since you've nested the label inside the form field element you'll lose the styles of the label when you alter the structure of the elements in the Jade file. Let's look at the same form component written in the shallow nesting method.

```sh
.form
    display: block
    
    &__field
        width: 100%
        
    &__label
        font-weight: bold
    
    &__input
        border: 1px solid $color-one
```
Going with the shallow nesting method using the `&` will allow you to decouple the dependency between the HTML structure of a component and how the elements are styled in the Sass file. Now you can rearrange any element in your component and all the styles assigned to each element will remain (not that you'd ever want to do that, but it's nice to know there is that flexibility available to you).

#### Don't Nest Your Selectors
Another part to the philosophy of the shallow nesting method is that it discourages deep nesting of CSS selectors. Nesting makes sense, especially while you're writing your styles, but it doesn't promote readability and maintainability, which is one of the main reasons we try to avoid it at all cost.

## **3. Sass Formatting**
Here are some basic rules to keep in mind when writing Sass.

### **Grouping Order**
Beyond the css properties, please note the order of the various @-rules, directives, selectors, .etc that may go under a single root component/module. Below is the order in which these should be written from top to bottom.

```sh
@extend
+include
+breakpoint
&:nth-child
&:before and &:after
&--modifier or &.class
.parent &
```

### **Vertical Spacing**
When considering the previous list of css/sass elements please insert a line space between all elements except for @extends, +includes, and css properties. For example...

```sh
.element-one
    @extend %foo
    +bar
    margin: 1rem
     
     +breakpoint(medium)
        display: none
    
    &:first-child
        margin-right: 0

.element-two
    display: inline-block
    
    &--selected
        border: 1px solid $color-two
    
    .parent-element &
        display: none
```

## **4. Comments**
If possible, make sure to add comments in your Sass file as you write, especially for components. In general, comments can serve as a great visual indicator for grouping similar elements together for better readability and maintainability. Use comments with other developers in mind and clearly show blocks of code.  

*As a side note, we will never use the comment format /\* \*/ simply because they add more to the overall file size and you'll only need comments when working with .sass files and never with .css files.*

### **Basic Formats**

```sh
// Grouped Selectors ////////////////////////////////////

// End Groupd Selectors .................................


// single selector //

.block__element // Property specific comment
```

### **Grouped Selectors Comments**  
-- Section off groups of selectors in a component to make it easier to visually see things that are related. For example...

```sh
// Mobile Search Filters /////////////////////////////////

&__mobile-search-input
    font-size: 1rem

&__mobile-search-select
    border: 1px solid $color-one

// End Mobile Search Filters .............................


// Global Search Button //////////////////////////////////

&__search-button
    display: inline-block
    height: 1rem
    
    input[type="submit"]
        font-size: 1rem

// End Global Search Button ..............................

```

-- Make sure to add a `//.....` at the end of the group to signify when things ends.  
-- Be sure to add a space after the section title and before the ending marker.  
-- Insert 2 line spaces in between grouped selectors.


### **Single Selector Comments**  
-- To add more clarity to smaller pieces of selectors within a group, use a comment title above the selector like so.

```sh
.nav
    margin: 2rem
    
    &__item
        background-color: $color-one
        font-size: 1.25rem
        
        // mobile style //
        +breakpoint(small only)
            font-size: 0.8rem
        
    // active-state //
    &__item--active
        background: $color-two
```

### **Property Specific Comments**  
-- Comments pertaining to a specific property should be placed immediately after the value and not above or below the property like so. 

```sh
.element
    width: 12rem // Comment about this property
```

## **Conclusion**

This is a highly opiniated Sass guideline and may not always work 100% of the time throughout our projects. Some things may need to change more than others at certain times. Take all of these guidelines and tips as a starting point for all your Sass ventures. Remember, clean, elegant code is not just beautiful and awesome, but it makes everyone's lives a little easier. It's the little things we do for each other that make this world a better place. I encourage all of you to put in your own love and affection into your code to make the HS dev team as awesome as it can be.

**p.s.**
*We want to make the dev department a safe place for comments, ideas, concerns and straight-up opinions. If you think this guide can be better or if you've got some other craaazy idea, make your stance and bring your knowledge to the team. Together, let's make great code.*

[Sass linter]: <https://github.com/sasstools/sass-lint>
[this page]: <https://github.com/sasstools/sass-lint/blob/develop/lib/config/property-sort-orders/concentric.yml>
[Get BEM]: <http://getbem.com/>
[here]: <http://ecss.io/preface.html>